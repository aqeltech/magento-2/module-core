# Changelog

## 1.1.2 - 02 Sep 2024

- Fix: Helper/Config Class name Typo fix

## 1.1.1 - 02 Sep 2024

- Fix: Helper/Config Class name Typo fix

## 1.1.0 - 01 Sep 2024

- Added: Config Helper where it can be used by other modules to grap config values

## 1.0.1 - 25 Aug 2024

- Added: Assets Media Directory

## 1.0.0 - 22 June 2024

- Initial Commit
