<?php

namespace AQELTech\Core\Helper;

use Magento\Framework\App\Config\ScopeConfigInterface;

class Config
{

    public function __construct(
        protected ScopeConfigInterface $scopeConfig
    ) {}

    public function getValue($value)
    {
        return $this->scopeConfig->getValue($value);
    }
}
